$(document).ready(function(){
    $("#mycarousel").carousel({interval:2000});
    $("#carousel-button").click(function()
    {
        if($("#carousel-button").children("span").hasClass("fa-play"))
        {
            $("#mycarousel").carousel('pause');
            $("#carousel-button").children("span").removeClass("fa-play");
            $("#carousel-button").children("span").addClass("fa-pause");
        }
        else  if($("#carousel-button").children("span").hasClass("fa-pause"))
        {
            $("#mycarousel").carousel('cycle');
            $("#carousel-button").children("span").removeClass("fa-pause");
            $("#carousel-button").children("span").addClass("fa-play");
        }
    }
    );
});


$("#loginbtn").click(function(){
    $('#loginModal').modal('show')
});
$("#close").click(function(){
$('#loginModal').modal('hide')
});
$("#close_btn").click(function(){
$('#loginModal').modal('hide')
});
$("#close_sign").click(function(){
$('#loginModal').modal('hide')
});

$("#reserve_table_btn").click(function(){
    $('#ReserveModal').modal('show')
});
$("#close_reseve_modal").click(function(){
$('#ReserveModal').modal('hide')
});
$("#close_can_reserve").click(function(){
$('#ReserveModal').modal('hide')
});
$("#close_reseve_btn").click(function(){
$('#ReserveModal').modal('hide')
});